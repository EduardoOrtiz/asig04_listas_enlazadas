/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/
#include <iostream>
using namespace std;

#ifndef bicola
#define bicola

class Node
{
	public:
		int element;
		Node* next;
		Node* prev;
		
		Node(int a = 0, Node* n = NULL, Node* b = NULL)
		{
			element = a;
			next = n;
			prev = b;
		}
};


/*=============================================================================
Let L = (1, 2, 3, 4, 5) be a double linked list.

Then the front of this class points to 1 and the back points to 5.
=============================================================================*/
class Bicola
{
	protected:
		Node* front;
		Node* back;
		int mySize;
	
	public:
		Bicola();
		~Bicola();
		
		bool empty();
		void push_front(int);
		void push_back(int);
		void pop_front();
		void pop_back();
		int get_front();
		int get_back();
		
		bool operator==(Bicola&);
		void print(ostream&);
};

ostream & operator << (ostream& out, Bicola&);

#endif