/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include "Bicola.h"
#include <iostream>
using namespace std;

Bicola::Bicola()
{
	front = NULL;
	back = NULL;
	mySize = 0;
}

Bicola::~Bicola()
{
	while(!this->empty())
		this->pop_front();
}

bool Bicola::empty()
{
	return (mySize == 0)? true : false;
}

void Bicola::push_front(int data)
{
	Node* Inserting = new Node(data);

	if(this->empty())
	{
		front = Inserting;
		back = Inserting;
	}
	else
	{
		front->prev = Inserting;
		Inserting->next = front;
		Inserting->prev = NULL; //not really needed since constructor
		front = Inserting;
	}

	mySize++;
}

void Bicola::push_back(int data)
{
	Node* Inserting = new Node(data);

	if(this->empty())
	{
		front = Inserting;
		back = Inserting;
	}
	else
	{
		back->next = Inserting;
		Inserting->prev = back;
		Inserting->next = NULL; //again, not needed
		back = Inserting;
	}

	mySize++;
}

void Bicola::pop_front()
{
	if(this->empty()) return;

	Node* Nodeptr = front;
	front = front->next;

	if(front == NULL)	//one element in the list
	{
		delete Nodeptr;
		back = NULL;
	}
	else	//more than one element in the list
	{
		delete Nodeptr;
		front->prev = NULL;
	}

	mySize--;
}

void Bicola::pop_back()
{
	if(this->empty()) return;

	Node* Nodeptr = back;
	back = back->prev;

	if(back == NULL) //one element in the list
	{
		delete Nodeptr;
		front = NULL;
	}
	else
	{
		delete Nodeptr;
		back->next = NULL;
	}

	mySize--;
}

int Bicola::get_front()
{
	if(this->empty()) return 0; //NULL
	else
		return front->element;
}

int Bicola::get_back()
{
	if(this->empty()) return 0; //NULL
	else
		return back->element;
}

bool Bicola::operator==(Bicola& Right)
{
	if(mySize != Right.mySize) return false;

	Node* Leftptr = this->front;
	Node* Rightptr = Right.front;

	while(Leftptr != NULL)
	{
		if(Leftptr->element != Rightptr->element)
			return false;

		Leftptr = Leftptr->next;
		Rightptr = Rightptr->next;
	}

	return true;
}

void Bicola::print(ostream& out)
{
	if(this->empty())
	{
		out << "Empty";
		return;
	}
	else
	{
		Node* Nodeptr = front;

		while(Nodeptr != NULL)
		{
			out << Nodeptr->element << " ";

			Nodeptr = Nodeptr->next;
		}
	}
}

ostream & operator << (ostream& out, Bicola& R)
{
	R.print(out);
	return out;
}
