/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include "Bicola.h"
#include <cassert>
#include <iostream>
using namespace std;

void test_push();
void test_pop();
void test_get();
void test_print();

int main()
{
	test_push();
	test_pop();
	test_get();
	test_print();

	return 0;
}

void test_push()
{
	Bicola A, B;

	int Array_A[] = {1, 2, 3, 4, 5, 6, 7};
	int Array_B[] = {7, 6, 5, 4, 3, 2, 1};

	for(int c = 0; c < 7; c++)
	{
		A.push_back(Array_A[c]);
		B.push_front(Array_B[c]);
	}

	cout << "Testing push_front and push_back..." << endl;

	assert(A == B);

	cout << "Passed!" << endl;
}

void test_pop()
{
	Bicola A, B;

	int Array_A[] = {2, 3, 4, 5, 6};
	int Array_B[] = {1, 2, 3, 4, 5, 6, 7};

	for(int c = 0; c < 5; c++)
		A.push_back(Array_A[c]);

	for(int c = 0; c < 7; c++)
		B.push_back(Array_B[c]);

	B.pop_back();
	B.pop_front();

	cout << "Testing pop_front and pop_back..." << endl;

	assert(A == B);

	cout << "Passed!" << endl;
}

void test_get()
{
	Bicola A, B;

	int Array[] = {1, 2, 3, 4, 5, 6, 7};
	
	for(int c = 0; c < 7; c++)
	{
		A.push_back(Array[c]);
		B.push_front(Array[c]);
	}

	cout << "Testing get_back and get_front..." << endl;

	assert(A.get_front() == B.get_back());

	cout << "Passed!" << endl;
}

void test_print()
{
	Bicola A;

	for(int c = 0; c < 20; c++)
		A.push_back(c);

	cout << "Printing a list of numbers from 0 to 19..." << endl
		 << A << endl;
}
